package com.rjr.apps.simplerecyclerview

/*
 * Copyright (C) 2019 RJR Apps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.RippleDrawable
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.annotation.IntRange
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.io.Serializable
import kotlin.math.ceil

open class SimpleRecyclerView<ItemType> : RecyclerView, Serializable {

    var adapter: Adapter? = null

    private var isPopulated = false

    enum class Orientation(val orientation: Int) {
        VERTICAL(1),
        HORIZONTAL(0);

        companion object {
            fun fromInt(orientation: Int) = when (orientation) {
                0 -> HORIZONTAL
                else -> VERTICAL
            }
        }
    }

    private lateinit var items: Array<ItemType>
    private lateinit var getLayoutRes: (itemPosition: ItemPosition<ItemType>) -> Int
    private lateinit var itemInit: (item: Item<ItemType>) -> Unit
    private var getClickViewRes: ((itemPosition: ItemPosition<ItemType>) -> Int?)? = null

    var onItemClick: ((item: Item<ItemType>) -> Unit)? = null
    var onItemLongClick: ((item: Item<ItemType>) -> Boolean)? = null

    @IntRange(from = 0) private var itemPadding = DEFAULT_ITEM_SPACING
    @IntRange(from = 0) private var gridLayoutSpans = DEFAULT_GRID_LAYOUT_SPANS
    private var orientation = DEFAULT_ORIENTATION
    private var reverseLayout = DEFAULT_REVERSE_LAYOUT
    private var stackFromEnd = DEFAULT_STACK_FROM_END
    private var canScrollVertically = DEFAULT_CAN_SCROLL_VERTICALLY(orientation)
    private var canScrollHorizontally = DEFAULT_CAN_SCROLL_HORIZONTALLY(orientation)
    private var dividerEnabled = DEFAULT_DIVIDER_ENABLED
    @ColorInt
    private var dividerColor: Int = DEFAULT_DIVIDER_COLOR
    private var rippleEnabled = DEFAULT_RIPPLE_ENABLED
    @ColorInt private var rippleColor: Int? = DEFAULT_RIPPLE_COLOR

    private var divider: DividerItemDecoration? = null

    /**
     * Default values and class tag
     * */
    companion object {
        protected const val TAG = "SimpleRecyclerView"

        protected const val DEFAULT_ITEM_SPACING = 0
        protected const val DEFAULT_GRID_LAYOUT_SPANS = 0
        protected val DEFAULT_ORIENTATION = Orientation.VERTICAL
        protected const val DEFAULT_REVERSE_LAYOUT = false
        protected const val DEFAULT_STACK_FROM_END = false
        protected fun DEFAULT_CAN_SCROLL_VERTICALLY(orientation: Orientation) = orientation == Orientation.VERTICAL
        protected fun DEFAULT_CAN_SCROLL_HORIZONTALLY(orientation: Orientation) = orientation == Orientation.HORIZONTAL
        protected const val DEFAULT_DIVIDER_ENABLED = false
        @ColorInt protected val DEFAULT_DIVIDER_COLOR = Color.parseColor("#dcdcdc")
        protected const val DEFAULT_RIPPLE_ENABLED = true
        protected val DEFAULT_RIPPLE_COLOR = null
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) { initFromAttrs(context, attrs) }
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)  { initFromAttrs(context, attrs) }

    /**
     * Initialize from XML attributes
     * */
    private fun initFromAttrs(context: Context, attrs: AttributeSet?) {
        val typedArray = context.theme.obtainStyledAttributes(attrs ?: return, R.styleable.SimpleRecyclerView, 0, 0) ?: return
        try {
            itemPadding = typedArray.getDimensionPixelSize(R.styleable.SimpleRecyclerView_itemPadding, itemPadding)
            gridLayoutSpans = typedArray.getInt(R.styleable.SimpleRecyclerView_gridLayoutSpans, gridLayoutSpans)
            setOrientationInternal(Orientation.fromInt(typedArray.getInt(R.styleable.SimpleRecyclerView_orientation, orientation.orientation)))
            reverseLayout = typedArray.getBoolean(R.styleable.SimpleRecyclerView_reverseLayout, reverseLayout)
            stackFromEnd = typedArray.getBoolean(R.styleable.SimpleRecyclerView_stackFromEnd, stackFromEnd)
            canScrollVertically = if (typedArray.getInt(R.styleable.SimpleRecyclerView_canScrollVertically, -1) == -1) { canScrollVertically } else { typedArray.getInt(R.styleable.SimpleRecyclerView_canScrollVertically, -1) == 1 }
            canScrollHorizontally = if (typedArray.getInt(R.styleable.SimpleRecyclerView_canScrollHorizontally, -1) == -1) { canScrollHorizontally } else { typedArray.getInt(R.styleable.SimpleRecyclerView_canScrollHorizontally, -1) == 1 }
            dividerEnabled = typedArray.getBoolean(R.styleable.SimpleRecyclerView_dividerEnabled, dividerEnabled)
            dividerColor = typedArray.getColor(R.styleable.SimpleRecyclerView_dividerColor, dividerColor)
            rippleEnabled = typedArray.getBoolean(R.styleable.SimpleRecyclerView_rippleEnabled, rippleEnabled)
            rippleColor = typedArray.getColor(R.styleable.SimpleRecyclerView_rippleColor, rippleColor ?: -1)
            if (rippleColor == -1) { rippleColor = DEFAULT_RIPPLE_COLOR }
        } finally {
            typedArray.recycle()
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <ItemType> initialize() = this as SimpleRecyclerView<ItemType>

    /**
     * Populates the recycler view and returns the SimpleRecyclerView with the [ItemType] properly set.
     *
     * @param items - Populate the RecyclerView with these items.
     * @param getLayoutRes - Set a layout resource for the items. Different resource ids can be specified based on the item/position.
     * @param itemInit - Called in [Adapter.onBindViewHolder]. Use this to render your items.
     * @param getClickViewRes - Specify the resId for the view to be used for clicking, long clicking, and ripples
     *
     * @param onItemClick - Called when an item is clicked.
     * @param onItemLongClick - Called when an item is long-clicked.
     *
     * @param itemPadding - Dynamically sets padding on items to keep spacing between items consistent
     * @param gridLayoutSpans - When set to anything higher than 0, the [RecyclerView.LayoutManager] will be set to a [GridLayoutManager] with the spans set according to this Int.
     * @param orientation - Sets the RecyclerView to [RecyclerView.VERTICAL] or [RecyclerView.HORIZONTAL].
     * @param reverseLayout - Sets the [RecyclerView.LayoutManager] to reverse layout when true
     * @param stackFromEnd - Sets the [RecyclerView.LayoutManager] to stack from end when true
     * @param canScrollVertically - Sets [RecyclerView.LayoutManager.canScrollVertically]
     * @param canScrollHorizontally - Sets [RecyclerView.LayoutManager.canScrollHorizontally]
     * @param dividerEnabled - If true, the RecyclerView will show a divider between each item
     * @param dividerColor - Allows custom divider colors to be used
     * @param rippleEnabled - If true, items will show a ripple when tapped
     * @param rippleColor - Sets the ripple's color
     * */
    @JvmSynthetic
    open fun <ItemType> populate(
            items: Array<ItemType>,
            getLayoutRes: (itemPosition: ItemPosition<ItemType>) -> Int,
            itemInit: (item: Item<ItemType>) -> Unit,
            getClickViewRes: ((itemPosition: ItemPosition<ItemType>) -> Int?)? = null,

            onItemClick: ((item: Item<ItemType>) -> Unit)? = null,
            onItemLongClick: ((item: Item<ItemType>) -> Boolean)? = null,

            @IntRange(from = 0) itemPadding: Int = this.itemPadding,
            @IntRange(from = 0) gridLayoutSpans: Int = this.gridLayoutSpans,
            orientation: Orientation = this.orientation,
            reverseLayout: Boolean = this.reverseLayout,
            stackFromEnd: Boolean = this.stackFromEnd,
            canScrollVertically: Boolean = this.canScrollVertically,
            canScrollHorizontally: Boolean = this.canScrollHorizontally,
            dividerEnabled: Boolean = this.dividerEnabled,
            @ColorInt dividerColor: Int = this.dividerColor,
            rippleEnabled: Boolean = this.rippleEnabled,
            @ColorInt rippleColor: Int? = this.rippleColor
    ): SimpleRecyclerView<ItemType> = initialize<ItemType>().apply {
        this.populate(
            items,
            getLayoutRes,
            itemInit,
            getClickViewRes,

            onItemClick,
            onItemLongClick,

            itemPadding,
            gridLayoutSpans,
            orientation,
            reverseLayout,
            stackFromEnd,
            canScrollVertically,
            canScrollHorizontally,
            dividerEnabled,
            dividerColor,
            rippleEnabled,
            rippleColor)
    }

    /**
     * Populates the recycler view.
     *
     * @param items - Populate the RecyclerView with these items.
     * @param getLayoutRes - Set a layout resource for the items. Different resource ids can be specified based on the item/position.
     * @param itemInit - Called in [Adapter.onBindViewHolder]. Use this to render your items.
     * @param getClickViewRes - Specify the resId for the view to be used for clicking, long clicking, and ripples
     *
     * @param onItemClick - Called when an item is clicked.
     * @param onItemLongClick - Called when an item is long-clicked.
     *
     * @param itemPadding - Dynamically sets padding on items to keep spacing between items consistent
     * @param gridLayoutSpans - When set to anything higher than 0, the [RecyclerView.LayoutManager] will be set to a [GridLayoutManager] with the spans set according to this Int.
     * @param orientation - Sets the RecyclerView to [RecyclerView.VERTICAL] or [RecyclerView.HORIZONTAL].
     * @param reverseLayout - Sets the [RecyclerView.LayoutManager] to reverse layout when true
     * @param stackFromEnd - Sets the [RecyclerView.LayoutManager] to stack from end when true
     * @param canScrollVertically - Sets [RecyclerView.LayoutManager.canScrollVertically]
     * @param canScrollHorizontally - Sets [RecyclerView.LayoutManager.canScrollHorizontally]
     * @param dividerEnabled - If true, the RecyclerView will show a divider between each item
     * @param dividerColor - Allows custom divider colors to be used
     * @param rippleEnabled - If true, items will show a ripple when tapped
     * @param rippleColor - Sets the ripple's color
     * */
    @JvmSynthetic
    open fun populate(
            items: Array<ItemType>,
            getLayoutRes: (itemPosition: ItemPosition<ItemType>) -> Int,
            itemInit: (item: Item<ItemType>) -> Unit,
            getClickViewRes: ((itemPosition: ItemPosition<ItemType>) -> Int?)? = null,

            onItemClick: ((item: Item<ItemType>) -> Unit)? = null,
            onItemLongClick: ((item: Item<ItemType>) -> Boolean)? = null,

            @IntRange(from = 0) itemPadding: Int = this.itemPadding,
            @IntRange(from = 0) gridLayoutSpans: Int = this.gridLayoutSpans,
            orientation: Orientation = this.orientation,
            reverseLayout: Boolean = this.reverseLayout,
            stackFromEnd: Boolean = this.stackFromEnd,
            canScrollVertically: Boolean = this.canScrollVertically,
            canScrollHorizontally: Boolean = this.canScrollHorizontally,
            dividerEnabled: Boolean = this.dividerEnabled,
            @ColorInt dividerColor: Int = this.dividerColor,
            rippleEnabled: Boolean = this.rippleEnabled,
            @ColorInt rippleColor: Int? = this.rippleColor
    ) {
        this.items = items
        this.getLayoutRes = getLayoutRes
        this.itemInit = itemInit
        this.getClickViewRes = getClickViewRes

        this.onItemClick = onItemClick
        this.onItemLongClick = onItemLongClick

        this.itemPadding = itemPadding
        this.gridLayoutSpans = gridLayoutSpans
        setOrientationInternal(orientation)
        this.reverseLayout = reverseLayout
        this.stackFromEnd = stackFromEnd
        this.canScrollVertically = canScrollVertically
        this.canScrollHorizontally = canScrollHorizontally
        this.dividerEnabled = dividerEnabled
        if (dividerColor != 0) { this.dividerColor = dividerColor }
        this.rippleEnabled = rippleEnabled
        if (rippleColor != null) { this.rippleColor = rippleColor }

        init()
    }

    /**
     * Populates the recycler view using a [Builder]. The [ItemType] should be set via casting the SimpleRecyclerView or calling [initialize] before this is called.
     * */
    open fun populate(builder: Builder<ItemType>) {
        this.items = builder.items()
        this.getLayoutRes = builder::layoutRes
        this.itemInit = builder::itemInit
        this.getClickViewRes = builder::clickViewRes

        this.onItemClick = builder::onItemClick
        this.onItemLongClick = builder::onItemLongClick

        this.itemPadding = if (builder.itemPadding() == null) { this.itemPadding } else { builder.itemPadding()!! }
        this.gridLayoutSpans = if (builder.gridLayoutSpans() == null) { this.gridLayoutSpans } else { builder.gridLayoutSpans()!! }
        setOrientationInternal(if (builder.orientation() == null) { this.orientation } else { builder.orientation()!! })
        this.reverseLayout = if (builder.reverseLayout() == null) { this.reverseLayout } else { builder.reverseLayout()!! }
        this.stackFromEnd = if (builder.stackFromEnd() == null) { this.stackFromEnd } else { builder.stackFromEnd()!! }
        this.canScrollVertically = if (builder.canScrollVertically() == null) { this.canScrollVertically } else { builder.canScrollVertically()!! }
        this.canScrollHorizontally = if (builder.canScrollHorizontally() == null) { this.canScrollHorizontally } else { builder.canScrollHorizontally()!! }
        this.dividerEnabled = if (builder.dividerEnabled() == null) { this.dividerEnabled } else { builder.dividerEnabled()!! }
        this.dividerColor = if (builder.dividerColor() == null) { this.dividerColor } else { builder.dividerColor()!! }
        this.rippleEnabled = if (builder.rippleEnabled() == null) { this.rippleEnabled } else { builder.rippleEnabled()!! }
        this.rippleColor = if (builder.rippleColor() == null) { this.rippleColor } else { builder.rippleColor()!! }

        init()
    }

    /**
     * [items] - Populate the RecyclerView with these items.
     * [layoutRes] - Set a layout resource for the items. Different resource ids can be specified based on the item/position.
     * [itemInit] - Called in [Adapter.onBindViewHolder]. Use this to render your items.
     * [clickViewRes] - Specify the resId for the view to be used for clicking, long clicking, and ripples
     *
     * [onItemClick] - Called when an item is clicked.
     * [onItemLongClick] - Called when an item is long-clicked.
     *
     * [itemPadding] - Dynamically sets padding on items to keep spacing between items consistent
     * [gridLayoutSpans] - When set to anything higher than 0, the [RecyclerView.LayoutManager] will be set to a [GridLayoutManager] with the spans set according to this Int.
     * [orientation] - Sets the RecyclerView to [RecyclerView.VERTICAL] or [RecyclerView.HORIZONTAL].
     * [reverseLayout] - Sets the [RecyclerView.LayoutManager] to reverse layout when true
     * [stackFromEnd] - Sets the [RecyclerView.LayoutManager] to stack from end when true
     * [canScrollVertically] - Sets [RecyclerView.LayoutManager.canScrollVertically]
     * [canScrollHorizontally] - Sets [RecyclerView.LayoutManager.canScrollHorizontally]
     * [dividerEnabled] - If true, the RecyclerView will show a divider between each item
     * [dividerColor] - Allows custom divider colors to be used
     * [rippleEnabled] - If true, items will show a ripple when tapped
     * [rippleColor] - Sets the ripple's color
     * */
    @SuppressLint("Range")
    abstract class Builder<ItemType> {

        abstract fun items(): Array<ItemType>
        @LayoutRes abstract fun layoutRes(itemPosition: ItemPosition<ItemType>): Int
        abstract fun itemInit(item: Item<ItemType>)
        @LayoutRes open fun clickViewRes(itemPosition: ItemPosition<ItemType>): Int? = null

        open fun onItemClick(item: Item<ItemType>) {}
        open fun onItemLongClick(item: Item<ItemType>) = false

        @IntRange(from = 0) open fun itemPadding(): Int? = null
        @IntRange(from = 0) open fun gridLayoutSpans(): Int? = null
        open fun orientation(): Orientation? = null
        open fun reverseLayout(): Boolean? = null
        open fun stackFromEnd(): Boolean? = null
        open fun canScrollVertically(): Boolean? = null
        open fun canScrollHorizontally(): Boolean? = null
        open fun dividerEnabled(): Boolean? = null
        @ColorInt open fun dividerColor(): Int? = null
        open fun rippleEnabled(): Boolean? = null
        @ColorInt open fun rippleColor(): Int? = null

    }

    /**
     * Checks to see if either of the [populate] methods have been called yet
     * */
    fun isPopulated() = isPopulated

    /**
     * Gets an array of the items currently populating the RecyclerView
     * */
    fun getItems() = if (this::items.isInitialized) { items } else { null }

    /**
     * Sets [items] with a new array and notifies the adapter of new data
     * */
    fun setItems(items: Array<ItemType>) {
        this.items = items
        adapter?.setItems(items)
    }

    /**
     * Sets a new layout resource getter and refreshes the RecyclerView
     * */
    fun setLayoutRes(getLayoutRes: (itemPosition: ItemPosition<ItemType>) -> Int) {
        if (this.getLayoutRes == getLayoutRes) { return }
        this.getLayoutRes = getLayoutRes
        initIfPopulated()
    }

    /**
     * Sets a new [itemInit] function and refreshes the RecyclerView
     * */
    fun setItemInit(itemInit: (item: Item<ItemType>) -> Unit) {
        if (this.itemInit == itemInit) { return }
        this.itemInit = itemInit
        initIfPopulated()
    }

    /**
     * Sets [itemPadding] in pixels
     * */
    fun setItemPadding(itemPadding: Int) {
        this.itemPadding = itemPadding
        notifyDataSetChanged()
    }

    /**
     * Check if current using a [GridLayoutManager]
     * */
    fun isGridLayout() = gridLayoutSpans > 0

    /**
     * Sets [gridLayoutSpans] and refreshes the RecyclerView
     * */
    fun setGridLayoutSpans(spanCount: Int) {
        if (this.gridLayoutSpans == spanCount) { return }
        gridLayoutSpans = spanCount
        initIfPopulated()
    }

    /**
     * Check the RecyclerView's orientation
     * */
    fun getOrientation() = orientation

    /**
     * Sets [orientation] and refreshes the RecyclerView
     * */
    fun setOrientation(orientation: Orientation) {
        if (this.orientation == orientation) { return }
        setOrientation(orientation)
        initIfPopulated()
    }

    private fun setOrientationInternal(orientation: Orientation) {
        if (this.orientation == orientation) { return }
        this.orientation = orientation
        canScrollVertically = DEFAULT_CAN_SCROLL_VERTICALLY(orientation)
        canScrollHorizontally = DEFAULT_CAN_SCROLL_HORIZONTALLY(orientation)
    }

    /**
     * Sets [reverseLayout] and refreshes the RecyclerView
     * */
    fun setReverseLayout(reverseLayout: Boolean) {
        if (this.reverseLayout == reverseLayout) { return }
        this.reverseLayout = reverseLayout
        initIfPopulated()
    }

    /**
     * Sets [stackFromEnd] and refreshes the RecyclerView
     * */
    fun setStackFromEnd(stackFromEnd: Boolean) {
        if (this.stackFromEnd == stackFromEnd) { return }
        this.stackFromEnd = stackFromEnd
        initIfPopulated()
    }

    /**
     * Sets [canScrollVertically] and refreshes the RecyclerView
     * */
    fun setCanScrollVertically(canScrollVertically: Boolean) {
        if (this.canScrollVertically == canScrollVertically) { return }
        this.canScrollVertically = canScrollVertically;
        initIfPopulated()
    }

    /**
     * Sets [canScrollHorizontally] and refreshes the RecyclerView
     * */
    fun setCanScrollHorizontally(canScrollHorizontally: Boolean) {
        if (this.canScrollHorizontally == canScrollHorizontally) { return }
        this.canScrollHorizontally = canScrollHorizontally
        initIfPopulated()
    }

    /**
     * Sets [dividerEnabled] and refreshes the RecyclerView
     * */
    fun setDividerEnabled(dividerEnabled: Boolean) {
        if (this.dividerEnabled == dividerEnabled) { return }
        this.dividerEnabled = dividerEnabled
        initIfPopulated()
    }

    /**
     * Sets [dividerColor] and refreshes the RecyclerView if the divider is enabled
     * */
    fun setDividerColor(@ColorInt dividerColor: Int) {
        if (this.dividerColor == dividerColor) { return }
        this.dividerColor = dividerColor
        if (dividerEnabled) {
            initIfPopulated()
        }
    }

    /**
     * Sets [rippleEnabled] and refreshes the RecyclerView
     * */
    fun setRippleEnabled(rippleEnabled: Boolean) {
        if (this.rippleEnabled == rippleEnabled) { return }
        this.rippleEnabled = rippleEnabled
        initIfPopulated()
    }

    /**
     * Sets [rippleColor] and refreshes the RecyclerView if ripples are enabled
     * */
    fun setRippleColor(rippleColor: Int?) {
        if (this.rippleColor == rippleColor) { return }
        this.rippleColor = rippleColor
        if (rippleEnabled) {
            initIfPopulated()
        }
    }

    /**
     * Returns an [ItemPosition] created by the last item clicked
     * */
    fun getLastItemClicked() = adapter?.getLastItemClicked()
    fun setLastItemClicked(@IntRange(from = 0) position: Int) { adapter?.setLastItemClicked(position) }
    fun clearLastItemClicked() { adapter?.clearLastItemClicked() }

    /**
     * Returns an [ItemPosition] created by the item clicked before the last
     * */
    fun getPriorItemClicked() = adapter?.getPriorItemClicked()
    fun setPriorItemClicked(@IntRange(from = 0) position: Int) { adapter?.setPriorItemClicked(position) }
    fun clearPriorItemClicked() { adapter?.clearPriorItemClicked() }

    /**
     * Returns a HashMap of all selected items and their indexes
     * Example: LinkedHashMap<SELECTED_ITEM_INDEX, SELECTED_ITEM_OBJECT>
     * */
    fun getSelectedItems() = adapter?.selectedItems

    /**
     * Set a single item selected.
     * */
    fun setSelectedItem(selectedItem: Int) {
        adapter?.selectedItems?.clear()
        adapter?.selectedItems?.putAll(linkedMapOf<Int, ItemType>().also { it[selectedItem] = this.items[selectedItem] })
    }

    /** Set a single item selected and notify the changed items. */
    fun setSelectedItemAndNotify(selectedItem: Int) {
        val priorSelectedItems = linkedMapOf<Int, ItemType>().also { it.putAll(getSelectedItems() ?: return@also) }
        setSelectedItem(selectedItem)
        priorSelectedItems.keys.forEach { notifyItemChanged(it) }
        getSelectedItems()?.keys?.forEach { notifyItemChanged(it) }
    }

    /**
     * Set the selected items by passing in an array of position Ints to be selected.
     * */
    fun setSelectedItems(selectedItems: Array<Int>) {
        val items = linkedMapOf<Int, ItemType>()
        selectedItems.forEach { position -> items[position] = this.items[position] }
        adapter?.selectedItems?.clear()
        adapter?.selectedItems?.putAll(items)
    }

    /** Set the selected items by passing in an array of position Ints to be selected, and notify the changed items. */
    fun setSelectedItemsAndNotify(selectedItems: Array<Int>) {
        val priorSelectedItems = linkedMapOf<Int, ItemType>().also { it.putAll(getSelectedItems() ?: return@also) }
        setSelectedItems(selectedItems)
        priorSelectedItems.keys.forEach { notifyItemChanged(it) }
        getSelectedItems()?.keys?.forEach { notifyItemChanged(it) }
    }

    /**
     * Clear the selected items.
     * */
    fun clearSelectedItems() { adapter?.selectedItems?.clear() }

    fun notifyDataSetChanged() { adapter?.notifyDataSetChanged() }
    fun notifyItemChanged(@IntRange(from = 0) position: Int) { adapter?.notifyItemChanged(position) }
    fun notifyItemChanged(@IntRange(from = 0) position: Int, payload: Any?) { adapter?.notifyItemChanged(position, payload) }
    fun notifyItemInserted(@IntRange(from = 0) position: Int) { adapter?.notifyItemInserted(position) }
    fun notifyItemMoved(@IntRange(from = 0) fromPosition: Int, @IntRange(from = 0) toPostion: Int) { adapter?.notifyItemMoved(fromPosition, toPostion) }
    fun notifyItemRemoved(@IntRange(from = 0) position: Int) { adapter?.notifyItemRemoved(position) }
    fun notifyItemRangeChanged(@IntRange(from = 0) positionStart: Int, itemCount: Int) { adapter?.notifyItemRangeChanged(positionStart, itemCount) }
    fun notifyItemRangeChanged(@IntRange(from = 0) positionStart: Int, itemCount: Int, payload: Any?) { adapter?.notifyItemRangeChanged(positionStart, itemCount, payload) }
    fun notifyItemRangeInserted(@IntRange(from = 0) positionStart: Int, itemCount: Int) { adapter?.notifyItemRangeInserted(positionStart, itemCount) }
    fun notifyItemRangeRemoved(@IntRange(from = 0) positionStart: Int, itemCount: Int) { adapter?.notifyItemRangeRemoved(positionStart, itemCount) }

    fun callOnClick(position: Int) { adapter?.callOnClick(position) }
    fun performClick(position: Int) { adapter?.performClick(position) }
    fun performLongClick(position: Int) { adapter?.performLongClick(position) }

    @JvmSynthetic
    fun setFilter(filter: (constraint: String, items: Array<ItemType>) -> MutableList<ItemType>) { adapter?.setFilter(filter) }
    fun setFilter(filter: FilterResults) { adapter?.setFilter { constraint, items ->  filter.filter(constraint, items) } }

    abstract inner class FilterResults {
        abstract fun filter(constraint: String, items: Array<ItemType>): MutableList<ItemType>
    }

    fun filterItems(text: String) {
        if (adapter?.isNotFilterSet() == true) {
            throw Exception("Filter is not set! Please call setFilter() before calling filterItems().")
        }

        adapter?.filter?.filter(text)
    }

    private fun initIfPopulated() {
        if (isPopulated) {
            init()
        }
    }

    /**
     * Initializes the [RecyclerView.LayoutManager] / [Adapter] and populates the RecyclerView
     * */
    protected open fun init() {
        isPopulated = true

        val layoutManager: LayoutManager
        if (isGridLayout()) {
            val gridLayoutManager = object : GridLayoutManager(context, gridLayoutSpans, orientation.orientation, reverseLayout) {
                override fun canScrollVertically() = canScrollVertically
                override fun canScrollHorizontally() = canScrollHorizontally
            }

            gridLayoutManager.stackFromEnd = stackFromEnd
            layoutManager = gridLayoutManager
        } else {
            val linearLayoutManager = object : LinearLayoutManager(context, orientation.orientation, reverseLayout) {
                override fun canScrollVertically() = canScrollVertically
                override fun canScrollHorizontally() = canScrollHorizontally
            }

            linearLayoutManager.stackFromEnd = stackFromEnd
            layoutManager = linearLayoutManager
        }

        setLayoutManager(layoutManager)

        divider?.run { removeItemDecoration(this) }
        if (dividerEnabled) {
            val drawable = getDrawableFromAttrRes(android.R.attr.listDivider)
            drawable?.setColorFilter(dividerColor, PorterDuff.Mode.SRC)

            val divider = DividerItemDecoration(context, orientation.orientation)
            drawable?.run { divider.setDrawable(this) }
            addItemDecoration(divider)
            this.divider = divider
        }

        adapter = Adapter(items, getLayoutRes, getClickViewRes, itemInit)
        setAdapter(adapter)
    }

    /**
     * Standard [RecyclerView.Adapter] to be used
     * */
    open inner class Adapter(private var items: Array<ItemType>, private val getLayoutRes: (itemPosition: ItemPosition<ItemType>) -> Int, private val getClickViewRes: ((itemPosition: ItemPosition<ItemType>) -> Int?)?, private val itemInit: (item: Item<ItemType>) -> Unit) : RecyclerView.Adapter<Adapter.ViewHolder>(), Serializable, Filterable {

        var filteredItems = items.toMutableList()
        val selectedItems = linkedMapOf<Int, ItemType>()
        private var lastItemClicked: ItemPosition<ItemType>? = null
        private var priorItemClicked: ItemPosition<ItemType>? = null
        private var filterFunction: ((constraint: String, items: Array<ItemType>) -> List<ItemType>)? = null
        private val originalPadding = hashMapOf<Int, Array<Int>>()

        override fun getItemCount() = filteredItems.size

        override fun getItemViewType(position: Int) = getLayoutRes(ItemPosition(filteredItems[position], position))

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(viewType, parent, false))

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val clickView = getClickViewRes?.invoke(ItemPosition(filteredItems[position], position)).let { clickViewRes ->
                return@let when (clickViewRes) {
                    null -> {
                        holder.itemView.isFocusable = rippleEnabled || onItemClick != null || onItemLongClick != null
                        holder.itemView.isClickable = rippleEnabled || onItemClick != null
                        holder.itemView.isLongClickable = onItemLongClick != null
                        holder.itemView
                    }
                    else -> holder.itemView.findViewById(clickViewRes) ?: throw RuntimeException("View not found inside ViewHolder layout! Ensure the resId you are specifying in your SimpleRecyclerView's getClickViewRes is inside the layout for this item. Position: $position")
                }
            }

            if (itemPadding > 0) {
                if (!originalPadding.containsKey(position)) {
                    originalPadding[position] = arrayOf(holder.itemView.paddingLeft, holder.itemView.paddingTop, holder.itemView.paddingRight, holder.itemView.paddingBottom)
                }

                holder.itemView.setPadding(itemPadding, itemPadding, itemPadding, itemPadding)

                if (gridLayoutSpans < 2) {
                    when {
                        orientation == Orientation.VERTICAL && (!reverseLayout && position == 0 || reverseLayout && position == filteredItems.lastIndex) -> holder.itemView.setPadding(itemPadding, itemPadding, itemPadding, (itemPadding.toFloat() / 2f).toInt())
                        orientation == Orientation.VERTICAL && (!reverseLayout && position == filteredItems.lastIndex || reverseLayout && position == 0) -> holder.itemView.setPadding(itemPadding, (itemPadding.toFloat() / 2f).toInt(), itemPadding, itemPadding)
                        orientation == Orientation.VERTICAL -> holder.itemView.setPadding(itemPadding, (itemPadding.toFloat() / 2f).toInt(), itemPadding, (itemPadding.toFloat() / 2f).toInt())

                        orientation == Orientation.HORIZONTAL && (!reverseLayout && position == 0 || reverseLayout && position == filteredItems.lastIndex) -> holder.itemView.setPadding(itemPadding, itemPadding, (itemPadding.toFloat() / 2f).toInt(), itemPadding)
                        orientation == Orientation.HORIZONTAL && (!reverseLayout && position == filteredItems.lastIndex || reverseLayout && position == 0) -> holder.itemView.setPadding((itemPadding.toFloat() / 2f).toInt(), itemPadding, itemPadding, itemPadding)
                        orientation == Orientation.HORIZONTAL -> holder.itemView.setPadding((itemPadding.toFloat() / 2f).toInt(), itemPadding, (itemPadding.toFloat() / 2f).toInt(), itemPadding)
                    }
                } else {
                    val column = when (orientation) {
                        Orientation.VERTICAL -> position % gridLayoutSpans
                        Orientation.HORIZONTAL -> (position - position % gridLayoutSpans) / gridLayoutSpans
                    }

                    val row = when (orientation) {
                        Orientation.VERTICAL -> (position - position % gridLayoutSpans) / gridLayoutSpans
                        Orientation.HORIZONTAL -> position % gridLayoutSpans
                    }

                    val lastColumn = when (orientation) {
                        Orientation.VERTICAL -> gridLayoutSpans
                        Orientation.HORIZONTAL -> ceil(filteredItems.size.toDouble() / gridLayoutSpans.toDouble()).toInt()
                    } - 1

                    val lastRow = when (orientation) {
                        Orientation.VERTICAL -> ceil(filteredItems.size.toDouble() / gridLayoutSpans.toDouble()).toInt()
                        Orientation.HORIZONTAL -> gridLayoutSpans
                    } - 1

                    var left = itemPadding / 2
                    var top = itemPadding / 2
                    var right = itemPadding / 2
                    var bottom = itemPadding / 2

                    if (column == 0) {
                        when {
                            orientation == Orientation.VERTICAL || orientation == Orientation.HORIZONTAL && !reverseLayout -> left *= 2
                            orientation == Orientation.HORIZONTAL && reverseLayout -> right *= 2
                        }
                    }
                    if (column == lastColumn) {
                        when {
                            orientation == Orientation.VERTICAL || orientation == Orientation.HORIZONTAL && !reverseLayout -> right *= 2
                            orientation == Orientation.HORIZONTAL && reverseLayout -> left *= 2
                        }
                    }

                    if (row == 0) {
                        when {
                            orientation == Orientation.VERTICAL && !reverseLayout || orientation == Orientation.HORIZONTAL -> top *= 2
                            orientation == Orientation.VERTICAL && reverseLayout || orientation == Orientation.HORIZONTAL -> bottom *= 2
                        }
                    }
                    if (row == lastRow) {
                        when {
                            orientation == Orientation.VERTICAL && !reverseLayout || orientation == Orientation.HORIZONTAL -> bottom *= 2
                            orientation == Orientation.VERTICAL && reverseLayout || orientation == Orientation.HORIZONTAL -> top *= 2
                        }
                    }

                    holder.itemView.setPadding(left, top, right, bottom)
                }
            } else {
                originalPadding[position].let { when (it) {
                    null -> holder.itemView.setPadding(0, 0, 0, 0)
                    else -> holder.itemView.setPadding(it[0], it[1], it[2], it[3])
                }}
            }

            itemInit(Item(holder.itemInnerView, getItemViewType(position), filteredItems[position], position))

            if (rippleEnabled) {
                val rippleColor = rippleColor
                when {
                    Build.VERSION.SDK_INT >= 23 -> {
                        clickView.foreground = RippleDrawable(ColorStateList.valueOf(rippleColor ?: getColorFromAttrRes(android.R.attr.colorControlHighlight)), clickView.foreground, when (getClickViewRes?.invoke(ItemPosition(filteredItems[position], position))) {
                            null -> null
                            else -> clickView.background
                        })
                    }
                    Build.VERSION.SDK_INT >= 21 -> {
                        clickView.background = RippleDrawable(ColorStateList.valueOf(rippleColor ?: getColorFromAttrRes(android.R.attr.colorControlHighlight)), clickView.background, when (getClickViewRes?.invoke(ItemPosition(filteredItems[position], position))) {
                            null -> null
                            else -> clickView.background
                        })
                    }
                    else -> clickView.background = getDrawableFromAttrRes(android.R.attr.selectableItemBackground)
                }
            }

            clickView.setOnClickListener {
                onItemClick?.let { onItemClick ->
                    itemTapped(position, filteredItems[position])
                    onItemClick.invoke(Item(holder.itemInnerView, getItemViewType(position), filteredItems[position], position))
                }
            }

            clickView.setOnLongClickListener {
                return@setOnLongClickListener onItemLongClick?.let { onItemLongClick ->
                    itemTapped(position, filteredItems[position])
                    onItemLongClick.invoke(Item(holder.itemInnerView, getItemViewType(position), filteredItems[position], position))
                } ?: false
            }
        }

        @Suppress("UNCHECKED_CAST")
        override fun getFilter() = object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                var filteredResults = items.toMutableList()

                filterFunction?.let { filterFunction ->
                    if (constraint.isNotEmpty()) {
                        filteredResults = filterFunction(constraint.toString().toLowerCase(), items).toMutableList()
                    }
                }

                val results = FilterResults()
                results.values = filteredResults
                return results
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                filteredItems = results.values as MutableList<ItemType>
                notifyDataSetChanged()
            }
        }

        fun setFilter(filter: (constraint: String, items: Array<ItemType>) -> MutableList<ItemType>) { filterFunction = filter }
        fun isFilterSet() = filterFunction != null
        fun isNotFilterSet() = filterFunction == null

        private fun itemTapped(position: Int, item: ItemType) {
            priorItemClicked = lastItemClicked
            lastItemClicked = ItemPosition(filteredItems[position], position)

            if (selectedItems.contains(position)) {
                selectedItems.remove(position)
                return
            }

            selectedItems[position] = item
        }

        fun setItems(items: Array<ItemType>) {
            clearSelectedItems()
            clearLastItemClicked()
            clearPriorItemClicked()
            this.items = items
            this.filteredItems = items.toMutableList()
            notifyDataSetChanged()
        }

        fun callOnClick(position: Int) {
            getViewHolderForPosition(position)?.callOnClick()
        }

        fun performClick(position: Int) {
            getViewHolderForPosition(position)?.performClick()
        }

        fun performLongClick(position: Int) {
            getViewHolderForPosition(position)?.performLongClick()
        }

        private fun getViewHolderForPosition(position: Int): View? {
            val holder = findViewHolderForAdapterPosition(position) ?: return null
            return getClickViewRes?.invoke(ItemPosition(filteredItems[position], position)).let { clickViewRes ->
                return@let when (clickViewRes) {
                    null -> {
                        holder.itemView.isFocusable = rippleEnabled || onItemClick != null || onItemLongClick != null
                        holder.itemView.isClickable = rippleEnabled || onItemClick != null
                        holder.itemView.isLongClickable = onItemLongClick != null
                        holder.itemView
                    }
                    else -> holder.itemView.findViewById(clickViewRes) ?: throw RuntimeException("View not found inside ViewHolder layout! Ensure the resId you are specifying in your SimpleRecyclerView's getClickViewRes is inside the layout for this item. Position: $position")
                }
            }
        }

        /**
         * Returns an [ItemPosition], containing the item's position and the object itself.
         * */
        fun getLastItemClicked() = lastItemClicked
        fun setLastItemClicked(@IntRange(from = 0) position: Int) { lastItemClicked = ItemPosition(filteredItems[position], position) }
        fun clearLastItemClicked() { lastItemClicked = null }

        /**
         * Returns an [ItemPosition], containing the item's position and the object itself.
         * */
        fun getPriorItemClicked() = priorItemClicked
        fun setPriorItemClicked(@IntRange(from = 0) position: Int) { priorItemClicked = ItemPosition(filteredItems[position], position) }
        fun clearPriorItemClicked() { priorItemClicked = null }

        inner class ViewHolder(val itemInnerView: View) : RecyclerView.ViewHolder(FrameLayout(context).also {
            it.layoutParams = itemInnerView.layoutParams
            it.clipChildren = false
            it.clipToPadding = false
            it.addView(itemInnerView)
        })

    }

    /**
     * Class to pass the item and position for [getLayoutRes]
     *
     * @param item - the item for listed for this [position]
     * @param position - integer to show the position
     *
     * */
    open class ItemPosition<ItemType>(val item: ItemType, val position: Int)

    /**
     * Class to pass the frameLayout, the frameLayout's resource int, the item, and position for [itemInit], [onItemClick], and [onItemLongClick]
     *
     * @param itemView - the [Adapter.ViewHolder.frameLayout] for this [position]
     * @param itemViewRes - the layout resource for this [itemView]
     * @param item - the item for listed for this [position]
     * @param position - integer to show the position
     *
     * */
    open class Item<ItemType>(val itemView: View, val itemViewRes: Int, val item: ItemType, val position: Int)


    @ColorInt
    protected open fun getColorFromAttrRes(attrRes: Int): Int {
        val typedArray = context.obtainStyledAttributes(intArrayOf(attrRes))
        val color = typedArray.getColor(0, 0)
        typedArray.recycle()
        return color
    }

    protected open fun getDrawableFromAttrRes(attrRes: Int): Drawable? {
        val typedArray = context.obtainStyledAttributes(intArrayOf(attrRes))
        val drawable = typedArray.getDrawable(0)
        typedArray.recycle()
        return drawable
    }

}
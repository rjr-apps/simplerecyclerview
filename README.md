#SimpleRecyclerView

For simple lists, I've always preferred building ListViews because RecyclerViews require a lot of work for such a simple View. But RecyclerViews are far better in pretty much every other aspect.

So for building simple lists, here's a `SimpleRecyclerView`.

#Download

First, add the JitPack repository in your root build.gradle file at the end of repositories:
```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

Then add the dependency in your app's build.gradle file:
```
dependencies {
    implementation 'org.bitbucket.rjr-apps:simplerecyclerview:1.1.2-10'
}
```

#Basic Usage

Include the view in your layout:

```
<com.rjr.apps.simplerecyclerview.SimpleRecyclerView
    android:id="@+id/simpleRecyclerView"
	android:layout_width="match_parent"
	android:layout_height="match_parent" />
```

Or build it programmatically:

**In Kotlin:**
```
val recyclerView = SimpleRecyclerView<ExampleModel>(context)
```

**In Java**
```
SimpleRecyclerView<ExampleModel> recyclerView = new SimpleRecyclerView<ExampleModel>(context);
```
.

This will create an empty `SimpleRecyclerView`. Adding items to it is easy, just call `SimpleRecyclerView.populate()` to initialize the View:

**In Kotlin:**
```
val items = arrayOf(ExampleModel("1"), ExampleModel("2"), ExampleModel("3"), ExampleModel("test"))

val recyclerView: SimpleRecyclerView<ExampleModel> = simpleRecyclerView.populate(
        items, // Populate the RecyclerView with an array of items (in this case, ExampleModels).
        getLayoutRes = { itemPosition ->
            // Set a layout resource for the items. Different resource ids can be specified based on the item/position. See SimpleRecyclerView.ItemPosition.
            android.R.layout.simple_list_item_1
        },
        itemInit = { it ->
            // Called in Adapter.onBindViewHolder. Use this to render your items. See SimpleRecyclerView.Item.
            it.itemView.findViewById<TextView>(android.R.id.text1).text = it.item.text
        }
)
```

**In Java:**
```
final ExampleModel[] items = new ExampleModel[] { new ExampleModel("1"), new ExampleModel("2"), new ExampleModel("3"), new ExampleModel("test") };
SimpleRecyclerView<ExampleModel> simpleRecyclerView = findViewById(R.id.simpleRecyclerView);

simpleRecyclerView = simpleRecyclerView.populate(new SimpleRecyclerView.Builder<ExampleModel>() {
    @NotNull
    @Override
    public ExampleModel[] items() {
        // Populate the RecyclerView with an array of items (in this case, ExampleModels).
        return items;
    }

    @Override
    public int layoutRes(@NotNull SimpleRecyclerView.ItemPosition<ExampleModel> itemPosition) {
        // Set a layout resource for the items. Different resource ids can be specified based on the item/position. See SimpleRecyclerView.ItemPosition.
        return android.R.layout.simple_list_item_1;
    }

    @Override
    public void itemInit(@NotNull SimpleRecyclerView.Item<ExampleModel> item) {
        // Called in Adapter.onBindViewHolder. Use this to render your items. See SimpleRecyclerView.Item.
        ((TextView)item.getItemView().findViewById(android.R.id.text1)).setText(item.getItem().getText());
    }
});
```
.

The builder method is also available in Kotlin, if preferred. You will need to cast your `SimpleRecyclerView` with the correct item type first though, or just use the return value from `SimpleRecyclerView.initialize<ItemType>()`.

##Multiple Item Layouts
You can specify a different item layout based on the item and/or position:

**In Kotlin**
```
recyclerView = simpleRecyclerView.populate(
        items,
        getLayoutRes = { itemPosition ->
            when {
                itemPosition.position == 1 -> android.R.layout.simple_expandable_list_item_1
                itemPosition.item.text == "test" -> android.R.layout.simple_dropdown_item_1line
                else -> android.R.layout.simple_list_item_1
            }
        },
        itemInit = { ... }
)
```
**In Java**
```
recyclerView.populate(new SimpleRecyclerView.Builder<ExampleModel>() {
    @NotNull
    @Override
    public ExampleModel[] items() {
        return items;
    }

    @Override
    public int layoutRes(@NotNull SimpleRecyclerView.ItemPosition<ExampleModel> itemPosition) {
        if (itemPosition.getPosition() == 1) {
            return android.R.layout.simple_expandable_list_item_1;
        } else if (itemPosition.getItem().getText().equals("test")) {
            return android.R.layout.simple_dropdown_item_1line;
        }
        
        return android.R.layout.simple_list_item_1;
    }

    @Override
    public void itemInit(@NotNull SimpleRecyclerView.Item<ExampleModel> item) {
        ...
    }
})
```

##Specifying the Click View
By default, it is assumed that `onItemClicked`, `onItemLongClicked`, and ripples (if enabled) are based on the root layout of the view holder. This can be changed to whatever view you want inside the item's layout:

**In Kotlin**
```
recyclerView = simpleRecyclerView.populate(
        items,
        getLayoutRes = { ... },      
        itemInit = { ... },
        getClickViewRes = { itemPosition ->
            when {
                itemPosition.position == 1 -> R.id.exampleItemInnerLayout
                itemPosition.item.text == "test" -> R.id.exampleItemOuterLayout
                else -> null // Will use root layout
            }
        },          
)
```
**In Java**
```
recyclerView.populate(new SimpleRecyclerView.Builder<ExampleModel>() {
    @NotNull
    @Override
    public ExampleModel[] items() {
        return items;
    }

    @Override
    public int layoutRes(@NotNull SimpleRecyclerView.ItemPosition<ExampleModel> itemPosition) {
        ...
    }

    @Override
    public void itemInit(@NotNull SimpleRecyclerView.Item<ExampleModel> item) {
        ...
    }
    
    @Nullable
    @Override
    public Integer clickViewRes(@NotNull SimpleRecyclerView.ItemPosition<ExampleModel> itemPosition) {
        if (itemPosition.getPosition() == 1) {
            return R.id.exampleItemInnerLayout;
        } else if (itemPosition.getItem().getText().equals("test")) {
            return R.id.exampleItemOuterLayout;
        }
        
        return null; // Will use root layout
    }   
})
```


##Accessing the Item Type
In Java, this is made easy by setting the type on the variable you're using for `findViewById()`. For example:
```
SimpleRecyclerView<ExampleModel> simpleRecyclerView = findViewById(R.id.simpleRecyclerView);
ExampleModel[] items = simpleRecyclerView.getItems();
```
However when using synthetic properties in Kotlin, you'll need to set a variable from the property and call `SimpleRecyclerView.initialize()` on the synthetic property:
```
val recyclerView = simpleRecyclerView.initialize<ExampleModel>()
val items: Array<ExampleModel> = recyclerView.getItems()
```
Alternatively, the `SimpleRecyclerView.populate()` functions also return with the proper item type:
```
val recyclerView = simpleRecyclerView.populate(...)
val items: Array<ExampleModel> = recyclerView.getItems()
```

##To Get a Callback on Item Click

In Kotlin, just set `onItemClick()` in `SimpleRecyclerView.populate()`:

```
onItemClick = { it ->
    // Called when an item is clicked. See SimpleRecyclerView.Item.
    Toast.makeText(context, it.item.text + " clicked!", Toast.LENGTH_SHORT).show()
},
onItemLongClick = { it ->
    // Called when an item is long-clicked. See SimpleRecyclerView.Item.
    Toast.makeText(context, it.item.text + " long-clicked!", Toast.LENGTH_SHORT).show()
}
```

In Java, just override `onItemClick()` in your builder:
```
@Override
public void onItemClick(@NotNull SimpleRecyclerView.Item<ExampleModel> item) {
    // Called when an item is clicked. See SimpleRecyclerView.Item.
    Toast.makeText(context, item.getItem().getText() + " clicked!", Toast.LENGTH_SHORT).show();
}

@Override
public boolean onItemLongClick(@NotNull SimpleRecyclerView.Item<ExampleModel> item) {
    // Called when an item is long-clicked. See SimpleRecyclerView.Item.
    Toast.makeText(context, item.getItem().getText() + " long-clicked!", Toast.LENGTH_SHORT).show();
    return true;
}
```


##To Get the Last Item Clicked

Just call `getLastItemClicked` on your SimpleRecyclerView object:

```
/**
 * Returns a SimpleRecyclerView.ItemPosition object, containing the item's position and the object itself.
 * */
val lastClickedtem: SimpleRecyclerView.ItemPosition<ExampleModel>? = recyclerView.getLastItemClicked()
```

##To Get the Prior Item Clicked

Just call `getPriorItemClicked` on your SimpleRecyclerView object. This will return the item that was clicked before the last:

```
/**
 * Returns a SimpleRecyclerView.ItemPosition object, containing the item's position and the object itself.
 * */
val priorSelectedItem: SimpleRecyclerView.ItemPosition<ExampleModel>? = recyclerView.getPriorItemClicked()
```

##To Get a List of Items Selected (For Multi-Selection)

Just call `getSelectedItems()` on your SimpleRecyclerView object:

```
/**
 * Returns a HashMap of all selected items and their indexes
 * Example: LinkedHashMap<SELECTED_ITEM_INDEX, SELECTED_ITEM_OBJECT>
 * */
val selectedItems: LinkedHashMap<Int, ExampleModel>? = recyclerView.getSelectedItems()
```

##To Use Search Filters

First, call `setFilter()` to set up how the filtering will work:

**In Kotlin**
```
recyclerView.setFilter { constraint, items ->
    val results = items.filter { exampleModel ->
        exampleModel.text.toLowerCase().contains(constraint.toLowerCase())
    }
    return@setFilter results.toMutableList()
}
```
**In Java**
```
recyclerView.setFilter(recyclerView.new FilterResults() {
    @NotNull
    @Override
    public List<ExampleModel> filter(@NotNull String constraint, @NotNull ExampleModel[] items) {
        List<ExampleModel> results = new ArrayList<>();
        for (ExampleModel model : items) {
            if (model.getText().toLowerCase().contains(constraint.toLowerCase())) {
                results.add(model);
            }
        }

        return results;
    }
});
```

Then just call `filterItems()` with your text filter. For example, to use with a SearchView on query text change:

```
searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(query: String) = false
    override fun onQueryTextChange(newText: String): Boolean {
        recyclerView.filterItems(newText)
        return true
    }
})
```

##Other Settings
There are several optional settings that can be set based on your preferences. These settings can be set in your XML layout, or programmatically in the `SimpleRecyclerView.populate()` functions or on the `SimpleRecyclerView` itself:
```
    <com.rjr.apps.simplerecyclerview.SimpleRecyclerView
        android:id="@+id/simpleRecyclerView"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:gridLayoutSpans="0"
        app:orientation="vertical"
        app:reverseLayout="false"
        app:stackFromEnd="false"
        app:canScrollVertically="True"
        app:canScrollHorizontally="False"
        app:dividerEnabled="false"
        app:dividerColor="@color/colorPrimary"
        app:rippleEnabled="true"
        app:rippleColor="@color/colorAccent" />
```

**- gridLayoutSpans:** When set to anything higher than 0, the LayoutManager will be set to a GridLayoutManager with the spans set according to this Int

**- orientation:**  Sets the RecyclerView to RecyclerView.VERTICAL or RecyclerView.HORIZONTAL

**- reverseLayout:** Sets the LayoutManager to reverse layout when true

**- stackFromEnd:** Sets the LayoutManager to stack from end when true

**- canScrollVertically:** Sets LayoutManager.canScrollVertically. In XML, must be set to "True" or "False" (capitalized first letter)

**- canScrollHorizontally:** Sets LayoutManager.canScrollHorizontally. In XML, must be set to "True" or "False" (capitalized first letter)

**- dividerEnabled:** If true, the RecyclerView will show a divider between each item

**- dividerColor:** Allows custom divider colors to be used

**- rippleEnabled:** If true, items will show a ripple when tapped

**- rippleColor:** Sets the ripple's color
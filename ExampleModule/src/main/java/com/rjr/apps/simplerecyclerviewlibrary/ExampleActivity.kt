package com.rjr.apps.simplerecyclerviewlibrary

/*
 * Copyright (C) 2019 RJR Apps
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.rjr.apps.simplerecyclerview.SimpleRecyclerView
import kotlinx.android.synthetic.main.activity_example.*

internal class ExampleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example)

        val items = arrayOf(ExampleModel("1"), ExampleModel("2"), ExampleModel("3"), ExampleModel("test"))

        // Optional, sets the ItemType to ExampleModel. Calling SimpleRecyclerView.populate() will also do this.
        var recyclerView = simpleRecyclerView.initialize<ExampleModel>()

        recyclerView = simpleRecyclerView.populate(
                items,
                getLayoutRes = { android.R.layout.simple_list_item_1 },
                itemInit = this::itemInit,
                onItemClick = this::onItemClick,
                onItemLongClick = this::onItemLongClick
        )

/*
        recyclerView = simpleRecyclerView.populate(
                items,
                getLayoutRes = { itemPosition ->
                    when {
                        itemPosition.position == 1 -> android.R.layout.simple_expandable_list_item_1
                        itemPosition.item.text == "test" -> android.R.layout.simple_dropdown_item_1line
                        else -> android.R.layout.simple_list_item_1
                    }
                },
                itemInit = this::itemInit,
                onItemClick = this::onItemClick,
                onItemLongClick = this::onItemLongClick
        )
*/

/*
        recyclerView = recyclerView.populate(object : SimpleRecyclerView.Builder<ExampleModel>() {
            override fun items() = items
            override fun layoutRes(itemPosition: SimpleRecyclerView.ItemPosition<ExampleModel>) = android.R.layout.simple_list_item_1
            override fun itemInit(item: SimpleRecyclerView.Item<ExampleModel>) { this@ExampleActivity::itemInit.invoke(item) }
            override fun onItemClick(item: SimpleRecyclerView.Item<ExampleModel>) { this@ExampleActivity::onItemClick.invoke(item) }
            override fun onItemLongClick(item: SimpleRecyclerView.Item<ExampleModel>): Boolean { return this@ExampleActivity::onItemLongClick.invoke(item) }
        })
*/

/*
        recyclerView = recyclerView.populate(object : SimpleRecyclerView.Builder<ExampleModel>() {
            override fun items() = items
            override fun layoutRes(itemPosition: SimpleRecyclerView.ItemPosition<ExampleModel>): Int {
                return when {
                    itemPosition.position == 1 -> android.R.layout.simple_expandable_list_item_1
                    itemPosition.item.text == "test" -> android.R.layout.simple_dropdown_item_1line
                    else -> android.R.layout.simple_list_item_1
                }
            }
            override fun itemInit(item: SimpleRecyclerView.Item<ExampleModel>) { this@ExampleActivity::itemInit.invoke(item) }
            override fun onItemClick(item: SimpleRecyclerView.Item<ExampleModel>) { this@ExampleActivity::onItemClick.invoke(item) }
            override fun onItemLongClick(item: SimpleRecyclerView.Item<ExampleModel>): Boolean { return this@ExampleActivity::onItemLongClick.invoke(item) }
        })
*/

/*
        recyclerView.setFilter { constraint, items ->
            val results = items.filter { exampleModel ->
                exampleModel.text.toLowerCase().contains(constraint.toLowerCase())
            }
            return@setFilter results.toMutableList()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String) = false
            override fun onQueryTextChange(newText: String): Boolean {
                recyclerView.filterItems(newText)
                return true
            }
        })
*/

    }

    private fun itemInit(item: SimpleRecyclerView.Item<ExampleModel>) { (item.itemView as TextView).text = item.item.text }
    private fun onItemClick(item: SimpleRecyclerView.Item<ExampleModel>) { Toast.makeText(this, item.item.text + " clicked!", Toast.LENGTH_SHORT).show() }
    private fun onItemLongClick(item: SimpleRecyclerView.Item<ExampleModel>): Boolean { Toast.makeText(this, item.item.text + " long-clicked!", Toast.LENGTH_SHORT).show(); return true }

}